import React, { useContext, useEffect } from 'react'
import { StyleSheet } from 'react-native'
import { Text, Button, Input, Form, Label, Item, Container } from "native-base";
import AuthForm from './AuthForm';
import { Context as AuthContext } from "../context/AuthContext";

const Signin = () => {
    const { signin, authStateChange } = useContext(AuthContext)

    useEffect(() => {
        authStateChange()
    }, [])

    return (
        <AuthForm submit={signin} /> 
    )
}

const styles = StyleSheet.create({

})


export default Signin
