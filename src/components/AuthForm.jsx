import React, { useState } from "react";
import { StyleSheet } from "react-native";
import { Text, Button, Input, Form, Label, Item, Container } from "native-base";

const AuthForm = ({ submit }) => {
    const [email, setEmail] = useState("");

    const [password, setPassword] = useState("");

    const formSubmit = () => {
        submit(email, password);
        setEmail("");
        setPassword("");
    };

    return (
        <Container style={styles.container}>
            <Form>
                <Item style={styles.input} floatingLabel>
                    <Label>Email</Label>
                    <Input
                        value={email}
                        onChangeText={setEmail}
                        autoCapitalize="none"
                        autoCorrect={false}
                    />
                </Item>
                <Item style={styles.input} floatingLabel>
                    <Label>Password</Label>
                    <Input
                        value={password}
                        onChangeText={setPassword}
                        autoCapitalize="none"
                        autoCorrect={false}
                        secureTextEntry
                    />
                </Item>
                <Button
                    style={styles.button}
                    bordered
                    info
                    onPress={formSubmit}
                >
                    <Text>Log in</Text>
                </Button>
            </Form>
        </Container>
    );
};

const styles = StyleSheet.create({
    container: {},
    input: {
        margin: 10
    },
    button: {
        width: 200,
        justifyContent: "center",
        alignSelf: "center",
        margin: 10
    }
});

export default AuthForm;
