import createDataContext from "./createDataContext";
import { SIGNIN, SIGNUP, SIGNOUT } from "../constance";
import firebase from "../config/firebase";

const authReducer = (state, action) => {
    switch (action.type) {
        case SIGNIN:
            return state;
        case SIGNUP:
            return state;
        case SIGNOUT:
            return state;
        default:
            return state;
    }
};

const signin = dispatch => (email, password) => {
    firebase
        .auth()
        .signInWithEmailAndPassword(email, password)
        .then(res => {
            const user = res.user;
            console.log(user);
        })
        .catch(err => {
            console.log(err);
        });
};

// const getToken = dispatch => () => {
//     firebase
//         .auth()
//         .currentUser.getIdToken(false)
//         .then(token => {
//             console.log(token);
//         })
//         .catch(err => {
//             console.log(err);
//         });
// };

const authStateChange = dispatch => () => {
    firebase.auth().onAuthStateChanged(user => {
        console.log(user);
    });
};

export const { Provider, Context } = createDataContext(
    authReducer,
    { signin, authStateChange },
    { token: null }
);
