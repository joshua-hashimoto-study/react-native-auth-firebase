import React, { useContext } from "react";
import { StyleSheet } from "react-native";
import { Header, Container } from "native-base";
import Signin from "./src/components/Signin";
import { Provider as AuthProvider } from "./src/context/AuthContext";

const App = () => {

    return (
        <Container style={styles.container}>
            <Header></Header>
            <Signin />
        </Container>
    );
};

const styles = StyleSheet.create({
    container: {}
});

export default () => {
    return (
        <AuthProvider>
            <App /> 
        </AuthProvider>
    )
}
